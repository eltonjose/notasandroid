package br.com.ueg.Notas.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import br.com.ueg.Notas.extras.Constantes;

/**
 * Created with IntelliJ IDEA.
 * User: elton
 * Date: 19/09/13
 * Time: 12:10
 * To change this template use File | Settings | File Templates.
 */
public class NotasDbHelper extends SQLiteOpenHelper {


    private final String CRIAR_TABELA =
            "create table "+ Constantes.TB_NOTAS +" ( "+
                    Constantes.COL_ID +" integer primary key autoincrement," +
                    Constantes.COL_TITULO +" text not null," +
                    Constantes.COL_CONTEUDO +" text not null);";

    public NotasDbHelper(Context context){
        super(context, Constantes.DB_NOME,null, Constantes.DB_VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CRIAR_TABELA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int antigo, int novo) {
        Log.w(Constantes.TAG, " de " + antigo + " para "
                + novo + ", tudo sera apagado");
        db.execSQL("DROP TABLE IF EXISTS "+ Constantes.TB_NOTAS);
        onCreate(db);
    }






}
