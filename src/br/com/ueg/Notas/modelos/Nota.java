package br.com.ueg.Notas.modelos;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created with IntelliJ IDEA.
 * User: elton
 * Date: 19/09/13
 * Time: 11:57
 * To change this template use File | Settings | File Templates.
 */
public class Nota implements Parcelable {

    private String titulo;
    private String conteudo;

    public Nota() {
    }

    public Nota(String titulo, String conteudo) {
        this.titulo = titulo;
        this.conteudo = conteudo;
    }

    public Nota(Parcel in) {
        readFromParcel(in);
    }


    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getConteudo() {
        return conteudo;
    }

    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }

    @Override
    public int describeContents() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getTitulo());
        parcel.writeString(getConteudo());
    }

    public void readFromParcel(Parcel parcel) {
        setTitulo(parcel.readString());
        setConteudo(parcel.readString());

    }

    public static final Parcelable.Creator<Nota> CREATOR = new Parcelable.Creator<Nota>() {

        public Nota createFromParcel(Parcel source) {

            return new Nota(source); // using parcelable constructor
        }

        @Override
        public Nota[] newArray(int i) {
            return new Nota[0];  //To change body of implemented methods use File | Settings | File Templates.
        }

    };

}
