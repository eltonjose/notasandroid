package br.com.ueg.Notas.adaptadores;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import br.com.ueg.Notas.helpers.NotasDbHelper;
import br.com.ueg.Notas.extras.Constantes;
import br.com.ueg.Notas.modelos.Nota;


/**
 * Created with IntelliJ IDEA.
 * User: elton
 * Date: 19/09/13
 * Time: 14:57
 * To change this template use File | Settings | File Templates.
 */
public class NotasDbAdapter {

    public SQLiteDatabase notasDatabase;
    public NotasDbHelper notasDbHelper;

    private final Context mContext;

    public NotasDbAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public NotasDbAdapter open() throws SQLException {
        notasDbHelper = new NotasDbHelper(mContext);
        notasDatabase = notasDbHelper.getWritableDatabase();
        return this;
    }

    public void close(){
        notasDatabase.close();
    }

    public long insereNota(Nota nota){
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constantes.COL_TITULO, nota.getTitulo());
        contentValues.put(Constantes.COL_CONTEUDO, nota.getConteudo());
        return notasDatabase.insert(Constantes.TB_NOTAS, null, contentValues);
    }

    public boolean deletaNota(long linhaID){
        return notasDatabase.delete(Constantes.TB_NOTAS, Constantes.COL_ID + "=" + linhaID, null) > 0;
    }

    public Cursor pegaTodasNotas(){
        return notasDatabase.query(Constantes.TB_NOTAS, new String[] {Constantes.COL_ID, Constantes.COL_TITULO,
                Constantes.COL_CONTEUDO}, null, null, null, null, null);
    }




}
