package br.com.ueg.Notas.adaptadores;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.ueg.Notas.R;
import br.com.ueg.Notas.extras.Constantes;

/**
 * Created with IntelliJ IDEA.
 * User: elton
 * Date: 20/09/13
 * Time: 18:18
 * To change this template use File | Settings | File Templates.
 */
public class NotaListItemAdapter extends BaseAdapter {

    private Context context;
    private Cursor cursor;

    private static LayoutInflater inflater = null;


    public NotaListItemAdapter(Context context, Cursor cursor) {
        this.context = context;
        this.cursor = cursor;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        if (cursor != null && cursor.getCount() > 0)
            return cursor.getCount();
        else
            return 0;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi = view;

        if (view == null) {
            vi = inflater.inflate(R.layout.linha_notas, null);
        }

        TextView tvId = (TextView) vi.findViewById(R.id.id);
        TextView tvTitulo = (TextView) vi.findViewById(R.id.titulo);
        TextView tvConteudo = (TextView) vi.findViewById(R.id.conteudo);

        long id = 0;
        String titulo = "";
        String conteudo = "";

        if(cursor.moveToPosition(i)){
            id = cursor.getLong(
                    cursor.getColumnIndex(
                            Constantes.COL_ID));
            titulo = cursor.getString(
                    cursor.getColumnIndex(
                            Constantes.COL_TITULO));
            conteudo = cursor.getString(
                    cursor.getColumnIndex(
                            Constantes.COL_CONTEUDO));

        }

        tvId.setText(id+"");
        tvTitulo.setText(titulo);
        tvConteudo.setText(conteudo);

        return vi;

    }
}
