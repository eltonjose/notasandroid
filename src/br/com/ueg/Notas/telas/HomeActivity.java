package br.com.ueg.Notas.telas;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import br.com.ueg.Notas.R;
import br.com.ueg.Notas.controles.NotasControler;

public class HomeActivity extends ListActivity {

    private NotasControler notasControler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_notas);

        notasControler = new NotasControler(this);

        Button botaoAdicionar = (Button) findViewById(R.id.botao_adicionar);

        botaoAdicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                criaNotaAutomaticamente();

            }
        });

//        notasControler.pegarTodasNotas();


    }

    @Override
    protected void onResume() {
        notasControler.pegarTodasNotas();
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;

    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_adicionar_nota:
                criaNota();
                return true;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    private void criaNota() {
        Intent intent = new Intent(HomeActivity.this,NotaActivity.class);
        startActivity(intent);
//        startActivityForResult(intent, Constantes.CRIANDO_NOTA);
    }

    private void criaNotaAutomaticamente() {
        notasControler.criaNotaAutomaticamente();
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if(requestCode == Constantes.CRIANDO_NOTA && resultCode == RESULT_OK){
//            notasControler.criaNota((Nota) data.getExtras().getParcelable("nota"));
//        }
//
//        super.onActivityResult(requestCode, resultCode, data);
//    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {

        TextView tvId = (TextView) v.findViewById(R.id.id);

//        notasControler.deletaNota(position);
        notasControler.deletaNota(Integer.parseInt(tvId.getText().toString()));

        super.onListItemClick(l, v, position, id);
    }
}
