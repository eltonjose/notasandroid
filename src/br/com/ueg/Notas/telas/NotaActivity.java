package br.com.ueg.Notas.telas;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import br.com.ueg.Notas.R;
import br.com.ueg.Notas.controles.NotasControler;
import br.com.ueg.Notas.modelos.Nota;

/**
 * Created with IntelliJ IDEA.
 * User: elton
 * Date: 20/09/13
 * Time: 12:07
 * To change this template use File | Settings | File Templates.
 */
public class NotaActivity extends Activity {

    private NotasControler notasControler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nota);

        notasControler = new NotasControler(NotaActivity.this);

        final TextView titulo = (TextView) findViewById(R.id.titulo);
        final TextView conteudo = (TextView) findViewById(R.id.conteudo);

        Button adicionarNota = (Button) findViewById(R.id.btn_adicionar) ;
        Button cancelar = (Button) findViewById(R.id.btn_cancelar);


        adicionarNota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notasControler.criaNota(titulo.getText().toString(),
                        conteudo.getText().toString());
                finish();
            }
        });

//        adicionarNota.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Nota nota = new Nota();
//                nota.setTitulo(titulo.getText().toString());
//                nota.setConteudo(conteudo.getText().toString());
//
//                notasControler.criaNota(nota);
//
////                Intent intent = new Intent();
////                Bundle bundle = new Bundle();
////                bundle.putParcelable("nota", nota);
////                intent.putExtras(bundle);
////
////                setResult(RESULT_OK, intent);
//                finish();
//
//            }
//        });


        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                setResult(RESULT_CANCELED);
                finish();
            }
        });



    }
}
