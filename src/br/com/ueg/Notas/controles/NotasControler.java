package br.com.ueg.Notas.controles;

import android.app.ListActivity;
import android.content.Context;
import android.database.Cursor;
import br.com.ueg.Notas.adaptadores.NotasDbAdapter;
import br.com.ueg.Notas.adaptadores.NotaListItemAdapter;
import br.com.ueg.Notas.modelos.Nota;

/**
 * Created with IntelliJ IDEA.
 * User: elton
 * Date: 23/09/13
 * Time: 11:07
 * To change this template use File | Settings | File Templates.
 */
public class NotasControler {

    private Context context;
    private NotasDbAdapter notasDbAdapter;
    private int n = 1;

    public NotasControler(Context context) {
        this.context = context;
        notasDbAdapter = new NotasDbAdapter(context);
        notasDbAdapter.open();
    }

    //    public void criaNota(Nota nota){
//        notasDbAdapter.insereNota(nota);
//        pegarTodasNotas();

    public void pegarTodasNotas(){
        if(context instanceof ListActivity) {
            Cursor c = notasDbAdapter.pegaTodasNotas();
            NotaListItemAdapter notes = new NotaListItemAdapter(context,c);
            ((ListActivity) context).setListAdapter(notes);
        }
    }

    public void criaNotaAutomaticamente() {
        Nota nota = new Nota("titulo"+n,"conteudo"+n);
        n++;
        notasDbAdapter.insereNota(nota);
        pegarTodasNotas();
    }

//    }

    public void criaNota(String  titulo,  String conteudo) {
        Nota nota = new Nota();
        nota.setTitulo(titulo);
        nota.setConteudo(conteudo);

        notasDbAdapter.insereNota(nota);
//        pegarTodasNotas();

    }

    public void deletaNota(int id){
        notasDbAdapter.deletaNota(id);
        pegarTodasNotas();
    }



}
